from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from flask_bcrypt import Bcrypt
from flask_login import LoginManager

postgres_local_base = f"postgresql://postgres:simanto@localhost:5433/new_usrdb"


app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI']=postgres_local_base
app.config['SECRET_KEY'] = 'jhf3983hf948f'
db = SQLAlchemy(app)
migrate = Migrate(app, db)
bcrypt = Bcrypt(app)
login_manager = LoginManager(app)
login_manager.login_view = "login_page"
login_manager.login_message_category = "info"

from market import routes